package henbangderen.java2puml.test;

import henbangderen.java2puml.test.composition.M;
import henbangderen.java2puml.test.composition.N;

public abstract class A_abstract
{
    M fieldFromAbstractParentClass = new M();
    N fieldFromAbstractParentClass2;

    public void setFieldFromAbstractParentClass2(N fieldFromAbstractParentClass2) {
        this.fieldFromAbstractParentClass2 = fieldFromAbstractParentClass2;
    }
}
