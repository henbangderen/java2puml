package henbangderen.java2puml.test;

import henbangderen.java2puml.test.aggregation.C;
import henbangderen.java2puml.test.composition.B;
import henbangderen.java2puml.test.dependency.*;

import java.util.ArrayList;
import java.util.List;

public class A extends A_parent implements A_interface, B_interace
{
    private B b = new B();
    private List<C> cs;

    public List<C> getCs()
    {
        return cs;
    }

    public void dependency(D dependency, List<J> dependency2)
    {

    }

    public void dependency()
    {
        E e = new E();
        F.go();
    }

    public void dependencyTypeInMethodCall()
    {
        E e = new E();
        e.call(new G());
    }

    public void logic()
    {
        if (new I().is())
        {
            new G();
        }
        else
        {
            int i = 10;
            K.run();
        }

        if (L.isGood())
        {
            String string = b.toString();
        }

        boolean whileB = true;
        while(whileB)
        {
            whileB = false;
            System.out.println(false);
        }
    }

    public void loops()
    {
        for (H h : new ArrayList<H>())
        {

        }
        for (int i = 0; i < 10; i++)
        {

        }
    }

    public void exception()
    {

    }

    public class InnerA
    {
        String a;
    }

    public String fieldFromParent()
    {
        return fieldFromAbstractParentClass.toString();
    }

    public String fieldFromParent2()
    {
        return fieldFromAbstractParentClass2.toString();
    }
}
