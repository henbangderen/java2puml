# java2puml

Converts java sources to the [plantUML](https://plantuml.com) files 

This tool helped me once to understand a complex legacy java project sadly not written in OOP manner. 
So the most important part was to conceive dependencies in big classes with many responsibilities.
For the sake of simplicity this tool shows dependencies of classes by dotted lines and association
(composition or aggregation) relations by solid ones. Fields and methods are omitted as unimportant for this task. 

Tested on jdk 1.7 sources

## Build
```
mvn clean package 
```
## Usage
* Download sources of a java project
* Download dependencies.
  Using for example maven [dependency:copy-dependencies](https://maven.apache.org/plugins/maven-dependency-plugin/copy-dependencies-mojo.html)
* Prepare a configuration file, see [example](examples/xstream.edn)
* Run the tool
```
java -Dhenbangderen.java2puml.configuration.path=PATH_TO_CONFIG -jar java2puml-1.0.0.jar
```
* Check the target directory set in the .edn config file
* Use the [plantuml](https://plantuml.com/starting) tool to generate a picture. For instance:
```
java -jar plantuml.1.2020.26.jar PATH_TO_TARGET_FOLDER
```
## Some thoughts
* You may polish result puml files manually to make a better looking picture 
* Pictures from huge projects will be unreadable. Whether simplify input files quantity or remove unimportant relations from target pumls

## Demo
* The puml files for the current tool: [java2puml](examples/puml/java2puml)
* The diagram for the current tool:

![](examples/puml/java2puml/dependencies.png)
  
* Xstream puml files: [xstream](examples/puml/xstream)
* Xstream diagram (large picture, better to download and view offline):
[dependencies.png](examples/puml/xstream/dependencies.png)

### License
Copyright © 2018 Roman Levakin. Distributed under the Eclipse Public License 1.0
