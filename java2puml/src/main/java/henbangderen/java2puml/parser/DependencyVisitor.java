package henbangderen.java2puml.parser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.stmt.ForeachStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.LocalClassDeclarationStmt;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.Type;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.resolution.UnsolvedSymbolException;
import henbangderen.java2puml.model.Unit;
import henbangderen.java2puml.model.UnitBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Что есть зависимость?
 * Нет среди объявленных полей
 * Параметр метода
 * Создание экземпляра внутри метода
 * Вызов статичного метода, поля
 * <p>
 * Получается простое правило: в коде используется тип, которого нет среди объявленных полей
 */
public class DependencyVisitor extends VoidVisitorAdapter
{
    private final Logger log = LoggerFactory.getLogger(getClass());
    private List<String> packagesToSkip = new ArrayList<>();

    private ExpressionVisitor expressionVisitor = new ExpressionVisitor();
    private ClassDeclaration classDeclaration = new ClassDeclaration();
    private Collection<Unit> fieldTypes = new HashSet<>();

    private Collection<Unit> parameterTypes = new HashSet<>();
    private Collection<Unit> expressionStmtTypes = new HashSet<>();
    private Collection<Unit> classOrInterfaceTypes = new HashSet<>();

    @Override
    public void visit(CompilationUnit compilationUnit, Object arg)
    {
        super.visit(compilationUnit, arg);
        classDeclaration.visit(compilationUnit);
    }

    @Override
    public void visit(LocalClassDeclarationStmt n, Object arg)
    {
        super.visit(n, arg);
        log.info("LocalClassDeclarationStmt: " + n);
    }

    @Override
    public void visit(FieldDeclaration fd, Object arg)
    {
        super.visit(fd, arg);
        Type type = fd.getElementType();
        String qualifiedName = "NOT_DEFINED";
        try
        {
            qualifiedName = type.resolve().describe();
        }
        catch (UnsupportedOperationException | UnsolvedSymbolException e)
        {
            log.error("May be not all dependencies are specified", e);
        }

        log.info("describeFieldDeclaration: " + qualifiedName);
        Unit unit = new UnitBuilder().build(qualifiedName);
        fieldTypes.add(unit);
/*        for (Node node : type.getChildNodes())
        {
            log.info("node: " + node);

            Unit unit = new Unit().setName(String.valueOf(node));
            fieldTypes.add(unit);
        }*/
    }

    @Override
    public void visit(ClassOrInterfaceType type, Object arg)
    {
        super.visit(type, arg);
        log.debug("resolve type: " + type);
        try
        {
            String qualifiedName = type.resolve().describe();
            log.debug("describeClassOrInterfaceType: " + qualifiedName);
            Unit unit = new UnitBuilder().build(qualifiedName);
            classOrInterfaceTypes.add(unit);
        }
        catch (UnsupportedOperationException e)
        {
            log.error("Error on resolving: " + type, e);
        }
        catch (UnsolvedSymbolException e)
        {
            log.error("Error on solving symbol", e);
        }
    }

    @Override
    public void visit(MethodDeclaration declaration, Object arg)
    {
        super.visit(declaration, arg);

        for (Node node : declaration.getType().getChildNodes())
        {
            if (node instanceof Type)
            {
                String described = "NOT_DEFINED";
                try
                {
                    described = ((Type) node).resolve().describe();
                }
                catch (UnsupportedOperationException e)
                {
                    log.error("ERROR!", e);
                }
                log.debug("describeMethodDeclaration: " + described);
                Unit unit = new UnitBuilder().build(described);
                parameterTypes.add(unit);
            }
        }
        for (Parameter param : declaration.getParameters())
        {
            Type type = param.getType();
            try
            {
                log.debug("describeParameter: " + type.resolve().describe());
            }
            catch (Exception e)
            {
                log.error("error", e);
            }
            for (Node node : type.getChildNodes())
            {
                log.debug("node2: " + node);
                Unit unit = new UnitBuilder().build(node.toString());
                parameterTypes.add(unit);
            }
        }
    }

    @Override
    public void visit(ExpressionStmt n, Object arg)
    {
        super.visit(n, arg);
        expressionVisitor.visit(n, expressionStmtTypes);
    }

    @Override
    public void visit(ForeachStmt n, Object arg)
    {
        super.visit(n, arg);
        expressionVisitor.visit(n, expressionStmtTypes);
    }

    @Override
    public void visit(IfStmt n, Object arg)
    {
        super.visit(n, arg);
        expressionVisitor.visit(n, expressionStmtTypes);
    }

    public Collection<Unit> getDependencies()
    {
        Set<Unit> result = new TreeSet<Unit>();

        Collection<Unit> collected = new ArrayList<>();
        collected.addAll(classOrInterfaceTypes);
        collected.addAll(expressionStmtTypes);

        for (Unit unit : collected)
        {
            if (isItToAdd(unit))
            {
                result.add(unit);
            }
        }
        return result;
    }

    private boolean isItToAdd(Unit unit)
    {
        if (fieldTypes.contains(unit))
        {
            return false;
        }

        for (Unit fieldType : fieldTypes)
        {
            if (fieldType.getDescribedType().contains(unit.getDescribedType()))
            {
                return false;
            }
        }

        if (classDeclaration.getImplementations().contains(unit))
        {
            return false;
        }

        if (classDeclaration.getExtension().equals(unit))
        {
            return false;
        }

        for (String packageToSkip : packagesToSkip)
        {
            if (unit.getPackage().contains(packageToSkip))
            {
                log.debug("skip unit by its package: " + unit);
                return false;
            }
        }

        return true;
    }

    public ClassDeclaration getClassDeclaration()
    {
        return classDeclaration;
    }

    public List<Unit> getFieldTypes()
    {
        return fieldTypes.stream().filter(unit -> !skipByPackage(unit)).collect(Collectors.toList());
    }

    public boolean skipByPackage(Unit unit)
    {
        for (String packageToSkip : packagesToSkip)
        {
            if (unit.getPackage().contains(packageToSkip))
            {
                log.debug("skip unit by its package: " + unit);
                return true;
            }
        }
        return false;
    }

    public DependencyVisitor setPackagesToSkip(List<String> packagesToSkip)
    {
        this.packagesToSkip = packagesToSkip;
        return this;
    }
}