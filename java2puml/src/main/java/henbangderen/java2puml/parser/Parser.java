package henbangderen.java2puml.parser;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import henbangderen.java2puml.model.ModelUnit;
import henbangderen.java2puml.model.ModelUnitCorrector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Parser
{
    private final Logger log = LoggerFactory.getLogger(getClass());
    private List<String> packagesToSkip = null;

    public ModelUnit parse(FileInputStream inputStream)
    {
        CompilationUnit cu = JavaParser.parse(inputStream);
        DependencyVisitor visitor = new DependencyVisitor();
        if (packagesToSkip == null)
        {
            throw new IllegalStateException("please set packages to skip!");
        }
        visitor.setPackagesToSkip(packagesToSkip);
        visitor.visit(cu, null);

        ModelUnit unit = new ModelUnit();
        unit.setRoot(visitor.getClassDeclaration().getRoot());
        unit.setExtension(visitor.getClassDeclaration().getExtension());
        unit.setAssociations(visitor.getFieldTypes());
        unit.setImplementations(visitor.getClassDeclaration().getImplementations());
        unit.setDependencies(visitor.getDependencies());
        log.debug("compilationUnit: " + cu);
        return unit;
    }

    public Collection<ModelUnit> parse(List<File> sources)
    {
        List<ModelUnit> shallowUnits = new ArrayList<>();
        for (File file : sources)
        {
            try
            {
                log.debug("parse: " + file);
                ModelUnit modelUnit = parse(new FileInputStream(file));
                shallowUnits.add(modelUnit);
            }
            catch (IOException e)
            {
                log.error("Failed to process '" + file.getAbsolutePath() + "'", e);
            }
        }
        return new ModelUnitCorrector().correct(shallowUnits);
    }

    public void setupTypeSolver(String sourcePath)
    {
        CombinedTypeSolver typeSolver = new CombinedTypeSolver();
        typeSolver.add(new JavaParserTypeSolver(new File(sourcePath)));
        typeSolver.add(new ReflectionTypeSolver());

        JavaSymbolSolver symbolSolver = new JavaSymbolSolver(typeSolver);
        JavaParser.getStaticConfiguration().setSymbolResolver(symbolSolver);
    }

    public void setPackagesToSkip(List<String> packagesToSkip)
    {
        this.packagesToSkip = packagesToSkip;
    }

    public CombinedTypeSolver setupTypeSolver(List<File> sourcePaths, List<File> pathsToJar)
    {
        CombinedTypeSolver typeSolver = new CombinedTypeSolver();
        for (File sourcePath : sourcePaths)
        {
            typeSolver.add(new JavaParserTypeSolver(sourcePath));
        }
        //JarTypeSolver jarTypeSolver = JarTypeSolver.getJarTypeSolver(
        for (File pathToJar : pathsToJar)
        {
            try
            {
                typeSolver.add(new JarTypeSolver(pathToJar));
                //JarTypeSolver.getJarTypeSolver(pathToJar.getAbsolutePath());
            }
            catch (IOException e)
            {
                log.error("error on adding jar type solver: '" + pathToJar + "'", e);
            }
        }
        typeSolver.add(new ReflectionTypeSolver());
        JavaSymbolSolver symbolSolver = new JavaSymbolSolver(typeSolver);
        JavaParser.getStaticConfiguration().setSymbolResolver(symbolSolver);
        return typeSolver;
    }
}
