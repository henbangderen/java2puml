package henbangderen.java2puml.parser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.resolution.UnsolvedSymbolException;
import com.github.javaparser.resolution.types.ResolvedType;
import henbangderen.java2puml.model.Unit;
import henbangderen.java2puml.model.UnitBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ClassDeclaration
{
    private final Logger log = LoggerFactory.getLogger(getClass());
    private UnitBuilder unitBuilder = new UnitBuilder();

    private Unit root = UnitBuilder.EMTPY_UNIT;
    private Unit extension = UnitBuilder.EMTPY_UNIT;;
    private List<Unit> implementations = new ArrayList<>();

    public void visit(CompilationUnit compilationUnit)
    {
        if (compilationUnit.getTypes().isEmpty())
        {
            log.warn("Can not get class declaration for: " + compilationUnit.toString());
            return;
        }

        Object type = compilationUnit.getTypes().get(0);
        if (type instanceof ClassOrInterfaceDeclaration)
        {
            ClassOrInterfaceDeclaration declaration = (ClassOrInterfaceDeclaration) type;
            root = getRoot(compilationUnit, declaration);
            extension = getExtension(declaration);
            implementations = getImplementations(declaration);
        }
        else
        {
            log.warn("NOT IMPLEMENTED: " + type.getClass().getName());
        }
    }

    private Unit getRoot(CompilationUnit compilationUnit, ClassOrInterfaceDeclaration declaration)
    {
        String packageName = "";
        if (compilationUnit.getPackageDeclaration().isPresent())
        {
            packageName = String.valueOf(compilationUnit.getPackageDeclaration().get().getName());
        }
        return  unitBuilder.build(packageName + "." + declaration.getNameAsString());
    }

    private Unit getExtension(ClassOrInterfaceDeclaration declaration)
    {
        if (!declaration.getExtendedTypes().isEmpty())
        {
            ResolvedType type = declaration.getExtendedTypes().get(0).resolve();
            return unitBuilder.build(type.describe());
        }
        return UnitBuilder.EMTPY_UNIT;
    }

    private List<Unit> getImplementations(ClassOrInterfaceDeclaration declaration)
    {
        List<Unit> implementations = new ArrayList<>();
        for (ClassOrInterfaceType type : declaration.getImplementedTypes())
        {
            String describe = "NOT_DEFINED";
            try
            {
                describe = type.resolve().describe();
            } catch (UnsolvedSymbolException e)
            {
                log.error("Can not solve type: " + type, e);
            }
            implementations.add(unitBuilder.build(describe));
        }
        return implementations;
    }

    public Unit getExtension()
    {
        return extension;
    }

    public List<Unit> getImplementations()
    {
        return implementations;
    }

    public Unit getRoot()
    {
        return root;
    }
}
