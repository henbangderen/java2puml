package henbangderen.java2puml.parser;

import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.resolution.types.ResolvedType;
import henbangderen.java2puml.model.Unit;
import henbangderen.java2puml.model.UnitBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;

public class ExpressionVisitor extends VoidVisitorAdapter
{
    private final Logger log = LoggerFactory.getLogger(getClass());

    public void visit(ExpressionStmt n, List<Unit> expressionStmtTypes)
    {
        super.visit(n, expressionStmtTypes);
        System.out.println("ExpressionStmt: " + n);
        System.out.println("Expression: " + n.getExpression());
    }

    @Override
    public void visit(MethodCallExpr n, Object arg)
    {
        super.visit(n, arg);
        Collection<Unit> expressionStmtTypes = (Collection<Unit>) arg;
//        log.info("method call: " + n);
    }

    @Override
    public void visit(NameExpr n, Object arg)
    {
        super.visit(n, arg);
        Collection<Unit> expressionStmtTypes = (Collection<Unit>) arg;

        ResolvedType type = null;
        try
        {
            type = n.calculateResolvedType();
        }
        catch (Exception e)
        {
            log.error("Can not calculate type for name expr: " + n, e);
            return;
        }

        log.info("name expr: " + n + " type: " + type);
        if (type.isReferenceType())
        {
            String described = type.describe();
            Unit unit = new UnitBuilder().build(described);
            expressionStmtTypes.add(unit);
        }
    }
}
