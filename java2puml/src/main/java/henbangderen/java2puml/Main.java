package henbangderen.java2puml;

import henbangderen.java2puml.configuration.Configuration;
import henbangderen.java2puml.configuration.ConfigurationFactory;

public class Main
{
    public static void main(String[] args)
    {
        Configuration configuration = ConfigurationFactory.getInstance().getConfiguration();
        Processor processor = new Processor();
        processor.setConfiguration(configuration);
        processor.process();
    }
}
