package henbangderen.java2puml.model;

import org.apache.commons.lang3.StringUtils;

public class UnitBuilder
{
    public static final Unit EMTPY_UNIT = new UnitBuilder().build(StringUtils.EMPTY);

    public Unit build(String describedType)
    {
        String packageSeparator = ".";
        String type = getType(describedType);

        Unit unit = new Unit();
        unit.setDescribedType(describedType);
        unit.setName(StringUtils.substringAfterLast(type, packageSeparator));
        unit.setPackage(StringUtils.substringBeforeLast(type, packageSeparator));
        unit.setQualifiedName(type);
        return unit;
    }

    private String getType(String describedType)
    {
        String startOfTypeParameters = "<";
        return StringUtils.substringBefore(describedType, startOfTypeParameters);
    }
}
