package henbangderen.java2puml.model;

public class Unit implements Comparable<Unit>
{
    private String describedType;
    private String name;
    private String pckg;
    private String qualifiedName;

    public String getName()
    {
        return name;
    }

    public Unit setName(String name)
    {
        this.name = name;
        return this;
    }

    public String getPackage()
    {
        return pckg;
    }

    public Unit setPackage(String pckg)
    {
        this.pckg = pckg;
        return this;
    }

    public String getPckg()
    {
        return pckg;
    }

    public Unit setPckg(String pckg)
    {
        this.pckg = pckg;
        return this;
    }

    public String getQualifiedName()
    {
        return qualifiedName;
    }

    public Unit setQualifiedName(String qualifiedName)
    {
        this.qualifiedName = qualifiedName;
        return this;
    }

    public String getDescribedType()
    {
        return describedType;
    }

    public Unit setDescribedType(String describedType)
    {
        this.describedType = describedType;
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Unit unit = (Unit) o;

        return getDescribedType().equals(unit.getDescribedType());
    }

    @Override
    public int hashCode()
    {
        return getDescribedType().hashCode();
    }

    @Override
    public int compareTo(Unit unit)
    {
        return this.getDescribedType().compareTo(unit.getDescribedType());
    }

    @Override
    public String toString()
    {
        return "Unit{" +
                "describedType='" + describedType + '\'' +
                ", name='" + name + '\'' +
                ", pckg='" + pckg + '\'' +
                ", qualifiedName='" + qualifiedName + '\'' +
                '}';
    }
}
