package henbangderen.java2puml.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 Finds children's fields which come from parents
 It reduces surplus dependency relations on an uml in case many java sources are analyzed at once
*/
public class ModelUnitCorrector
{
    private Logger log = LoggerFactory.getLogger(getClass());

    public Collection<ModelUnit> correct(List<ModelUnit> shallowUnits)
    {
        Map<String,String> childToParent = new HashMap<>();
        for(ModelUnit unit : shallowUnits)
        {
            childToParent.put(unit.getRoot().getQualifiedName(), unit.getExtension().getQualifiedName());
        }

        Map<String,List<String>> childToParents = new HashMap<>();
        for(ModelUnit unit : shallowUnits)
        {
            String id = unit.getRoot().getQualifiedName();
            childToParents.put(id, getParents(id, childToParent, new ArrayList<>()));
        }

        return correct(shallowUnits, childToParents);
    }

    private Collection<ModelUnit> correct(List<ModelUnit> shallowUnits,
                                    Map<String, List<String>> childToParents)
    {
        Map<String, ModelUnit> idToUnit = new HashMap<>();
        for (ModelUnit unit : shallowUnits)
        {
            idToUnit.put(unit.getRoot().getQualifiedName(), unit);
        }

        Collection<ModelUnit> result = new HashSet<>();
        for (Map.Entry<String, List<String>> entry : childToParents.entrySet())
        {
            String id = entry.getKey();
            List<String> parents = entry.getValue();
            if (parents.isEmpty())
            {
                ModelUnit old = idToUnit.get(id);
                result.add(old);
            }
            else
            {
                Collection<Unit> associations = new HashSet<>();
                for (String parentId : parents)
                {
                    ModelUnit parent = idToUnit.get(parentId);
                    if (null != parent)
                    {
                    associations.addAll(parent.getAssociations());
                    }
                }

                Collection<Unit> dependencies = new HashSet<>();
                for (Unit dep : idToUnit.get(id).getDependencies())
                {
                    for (Unit association : associations)
                    {
                        if (!dep.equals(association))
                        {
                            dependencies.add(dep);
                        }
                    }
                }
                ModelUnit newUnit = idToUnit.get(id).clone();
                newUnit.setDependencies(dependencies);
                result.add(newUnit);
            }
        }
        return result;
    }

    private List<String> getParents(String id, Map<String, String> db, List<String> accumulator)
    {
        if (null == db.get(id)) { return accumulator; }

        if (!db.get(id).isEmpty())
        {
            String parent = db.get(id);
            accumulator.add(parent);
            getParents(parent, db, accumulator);
        }
        return accumulator;
    }
}
