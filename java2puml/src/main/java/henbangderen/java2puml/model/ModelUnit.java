package henbangderen.java2puml.model;

import java.util.ArrayList;
import java.util.Collection;

//make immutable
public class ModelUnit
{
    private Unit root;
    private Unit extension;
    private Collection<Unit> implementations = new ArrayList<>();
    private Collection<Unit> associations = new ArrayList<>();
    private Collection<Unit> dependencies = new ArrayList<>();

    public Unit getRoot()
    {
        return root;
    }

    public ModelUnit setRoot(Unit root)
    {
        this.root = root;
        return this;
    }

    public Collection<Unit> getDependencies()
    {
        return dependencies;
    }

    public ModelUnit setDependencies(Collection<Unit> dependencies)
    {
        this.dependencies = dependencies;
        return this;
    }

    public Unit getExtension()
    {
        return extension;
    }

    public ModelUnit setExtension(Unit extension)
    {
        this.extension = extension;
        return this;
    }

    public Collection<Unit> getImplementations()
    {
        return implementations;
    }

    public ModelUnit setImplementations(Collection<Unit> implementations)
    {
        this.implementations = implementations;
        return this;
    }

    public Collection<Unit> getAssociations()
    {
        return associations;
    }

    public ModelUnit setAssociations(Collection<Unit> associations)
    {
        this.associations = associations;
        return this;
    }

    public ModelUnit clone()
    {
        ModelUnit clone = new ModelUnit();
        clone.setRoot(root);
        clone.setAssociations(associations);
        clone.setImplementations(implementations);
        clone.setExtension(extension);
        clone.setDependencies(dependencies);
        return clone;
    }

    @Override
    public String toString()
    {
        return "ModelUnit{" +
                "root=" + root +
                ", extension=" + extension +
                ", implementations=" + implementations +
                ", associations=" + associations +
                ", dependencies=" + dependencies +
                '}';
    }
}
