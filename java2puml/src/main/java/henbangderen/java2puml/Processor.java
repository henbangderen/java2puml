package henbangderen.java2puml;

import henbangderen.java2puml.configuration.Configuration;
import henbangderen.java2puml.model.ModelUnit;
import henbangderen.java2puml.parser.Parser;
import henbangderen.java2puml.renderer.PlantUmlRenderer;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Processor
{
    private Logger log = LoggerFactory.getLogger(getClass());
    private Configuration config;
    private Parser parser;

    public Processor setConfiguration(Configuration configuration)
    {
        config = configuration;
        parser = new Parser();
        parser.setupTypeSolver(
                config.getSourceDependencies(),
                config.getJarDependencies());
        parser.setPackagesToSkip(config.getPackagesToSkip());
        return this;
    }

    public void process()
    {
        List<String> mainPuml = Arrays.asList(
                "@startuml",
                "hide empty description",
                "left to right direction",
                //"top to bottom direction",
                getPumlParts(),
                "@enduml");

        File file = new File(config.getTargetPath() + "/dependencies.puml");
        String string = StringUtils.join(mainPuml, "\n");
        writeStringToFile(string, file);
    }

    private String getPumlParts()
    {
        List<String> pumlParts = new ArrayList<>();
        Collection<ModelUnit> units = parser.parse(config.getSources());
        for (ModelUnit unit : units)
        {
            PlantUmlRenderer renderer = new PlantUmlRenderer();
            renderer.setModelUnit(unit);
            if (renderer.getRootName().isEmpty())
            {
                log.warn("The unit will be skipped: " + unit);
                continue;
            }
            File result = new File(config.getTargetPath().getAbsolutePath(),
                          renderer.getRootName());
            writeStringToFile(renderer.render(), result);
            pumlParts.add("!include " + result.getName());
        }
        return StringUtils.join(pumlParts, "\n");
    }

    private void writeStringToFile(String string, File file)
    {
        try
        {
            FileUtils.writeStringToFile(file, string, "UTF-8");
        }
        catch (IOException e)
        {
            log.error("Error on writing file '" + file.getAbsolutePath() + "'", e);
        }
    }
}