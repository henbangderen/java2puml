package henbangderen.java2puml.renderer;

import henbangderen.java2puml.model.Unit;

import java.util.ArrayList;
import java.util.List;

public class ClassDeclarationRenderer extends Renderer
{
    @Override
    public List<String> render()
    {
        String klass = "class %s\n{\n}";
        List<String> result = new ArrayList<>();

        result.add(String.format(klass, modelUnit.getRoot().getQualifiedName()));
        result.add(implementations());
        return result;
    }

    private String implementations()
    {
        StringBuilder buffer = new StringBuilder();
        for (Unit unit : modelUnit.getImplementations())
        {
            buffer.append(String.format("interface %s\n", unit.getDescribedType()));
        }
        return buffer.toString();
    }
}
