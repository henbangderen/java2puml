package henbangderen.java2puml.renderer;

import java.util.Collections;
import java.util.List;

public class ExtensionRenderer extends Renderer
{
    @Override
    public List<String> render()
    {
        String format = "%s <|-- %s";
        if (isEmpty())
        {
            return Collections.emptyList();
        }
        return Collections.singletonList(String.format(format,
                 modelUnit.getExtension().getQualifiedName(),
                 modelUnit.getRoot().getQualifiedName()));
    }

    private boolean isEmpty()
    {
        return modelUnit.getExtension().getQualifiedName().isEmpty();
    }
}
