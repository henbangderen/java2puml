package henbangderen.java2puml.renderer;

import henbangderen.java2puml.model.Unit;

import java.util.ArrayList;
import java.util.List;

public class AssociationRenderer extends Renderer 
{
    @Override
    public List<String> render() 
    {
        String format = "%s --> %s";
        List<String> result = new ArrayList<>();
        String root = modelUnit.getRoot().getQualifiedName();

        for (Unit association : modelUnit.getAssociations())
        {
            String assoc = association.getQualifiedName();

            if (assoc.contains(root)) 
            {
                // todo: elaborate for more complicated cases
                result.add(String.format(format, root, root));
            } 
            else 
            {
                result.add(
                        String.format(format, modelUnit.getRoot().getQualifiedName(), association.getQualifiedName()));
            }
        }
        return result;
    }
}
