package henbangderen.java2puml.renderer;

import henbangderen.java2puml.model.Unit;

import java.util.ArrayList;
import java.util.List;

public class DependencyRenderer extends Renderer
{
    @Override
    public List<String> render()
    {
        String format = "%s ..> %s";
        List<String> result = new ArrayList<>();

        for (Unit implementation : modelUnit.getDependencies())
        {
            result.add(String.format(format,
                    modelUnit.getRoot().getQualifiedName(),
                    implementation.getQualifiedName()));
        }
        return result;
    }
}
