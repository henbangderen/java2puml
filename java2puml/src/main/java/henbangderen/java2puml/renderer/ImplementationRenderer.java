package henbangderen.java2puml.renderer;

import henbangderen.java2puml.model.Unit;

import java.util.ArrayList;
import java.util.List;

public class ImplementationRenderer extends Renderer
{
    @Override
    public List<String> render()
    {
        String format = "%s <|.. %s";
        List<String> result = new ArrayList<>();

        for (Unit implementation : modelUnit.getImplementations())
        {
            result.add(String.format(format,
                    implementation.getQualifiedName(),
                    modelUnit.getRoot().getQualifiedName()));
        }
        return result;
    }
}
