package henbangderen.java2puml.renderer;

import henbangderen.java2puml.model.ModelUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Renderer
{
    Logger log = LoggerFactory.getLogger(Renderer.class);
    ModelUnit modelUnit;

    public abstract <T> T render();

    public Renderer setModelUnit(ModelUnit modelUnit)
    {
        this.modelUnit = modelUnit;
        return this;
    }
}
