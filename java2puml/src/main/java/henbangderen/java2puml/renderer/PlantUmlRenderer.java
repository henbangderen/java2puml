package henbangderen.java2puml.renderer;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlantUmlRenderer extends Renderer
{
    @Override    
    public String render()
    {
        List<Renderer> renderers = Arrays.asList(
            new AssociationRenderer().setModelUnit(modelUnit),
            new ClassDeclarationRenderer().setModelUnit(modelUnit),
            new ExtensionRenderer().setModelUnit(modelUnit),
            new ImplementationRenderer().setModelUnit(modelUnit),
            new DependencyRenderer().setModelUnit(modelUnit));

        List<String> result = new ArrayList<>();
        for (Renderer renderer : renderers)
        {
            result.addAll(renderer.render());
        }
        return join(result);
    }

    public String join(List<String> list)
    {
        String result = StringUtils.join(list, '\n');
        return result;
    }

    public String wrapByHeaderFooter(String plantuml)
    {
        return "@startuml\n" + plantuml + "\n@enduml";
    }

    public String getRootName()
    {
        return modelUnit.getRoot().getQualifiedName();
    }
}