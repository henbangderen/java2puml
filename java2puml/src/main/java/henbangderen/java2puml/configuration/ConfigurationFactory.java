package henbangderen.java2puml.configuration;

import henbangderen.java2puml.Constants;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Collection;
import java.util.NoSuchElementException;

public class ConfigurationFactory
{
    private static volatile ConfigurationFactory instance = new ConfigurationFactory();
    private Logger log = LoggerFactory.getLogger(getClass());

    private ConfigurationFactory()
    {
    }

    public static ConfigurationFactory getInstance()
    {
        return instance;
    }

    public Configuration getConfiguration()
    {
        File config = discoverConfiguratonFile();
        log.info("configuration is discovered: " + config.getAbsolutePath());
        Configuration parsed = new EdnConfigurationParser().parse(config);
        return new ConfigurationResolver().resolve(parsed);
    }

    private File discoverConfiguratonFile()
    {
        String configPath = System.getProperty(Constants.CONFIGURATION_PATH_PROPERTY_NAME);
        if (configPath != null)
        {
            return new File(configPath);
        }

        Collection files = FileUtils.listFiles(
                new File("."),
                new NameFileFilter(Constants.CONFIGURATION_FILE_NAME),
                DirectoryFileFilter.DIRECTORY
        );

        if (files.isEmpty())
        {
            throw new NoSuchElementException(
                    "Configuration file is not found in the directory '" + new File(".").getAbsolutePath() + "' and its subdirs");
        }
        return (File) files.iterator().next();
    }
}
