package henbangderen.java2puml.configuration;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class Configuration
{
    private List<File> sources = Collections.EMPTY_LIST;
    private List<String> packagesToSkip = Collections.EMPTY_LIST;
    private List<String> skipSources = Collections.EMPTY_LIST;
    private List<File> jarDependencies = Collections.EMPTY_LIST;
    private List<File> sourceDependencies = Collections.EMPTY_LIST;
    private File targetPath = new File("target");

    public List<File> getSources()
    {
        return sources;
    }

    public Configuration setSources(List<File> sources)
    {
        this.sources = sources;
        return this;
    }

    public File getTargetPath()
    {
        return targetPath;
    }

    public Configuration setTargetPath(File targetPath)
    {
        this.targetPath = targetPath;
        return this;
    }

    public List<File> getJarDependencies()
    {
        return jarDependencies;
    }

    public Configuration setJarDependencies(List<File> jarDependencies)
    {
        this.jarDependencies = jarDependencies;
        return this;
    }

    public List<File> getSourceDependencies()
    {
        return sourceDependencies;
    }

    public Configuration setSourceDependencies(List<File> sourceDependencies)
    {
        this.sourceDependencies = sourceDependencies;
        return this;
    }

    public Configuration setSkipSources(List<String> skipSources)
    {
        this.skipSources = skipSources;
        return this;
    }

    public List<String> getSkipSources()
    {
        return skipSources;
    }

    public List<String> getPackagesToSkip()
    {
        return packagesToSkip;
    }

    public Configuration setPackagesToSkip(List<String> packagesToSkip)
    {
        this.packagesToSkip = packagesToSkip;
        return this;
    }

    @Override
    public String toString()
    {
        return "Configuration{" +
                "sources=" + sources +
                ", packagesToSkip=" + packagesToSkip +
                ", skipSources=" + skipSources +
                ", jarDependencies=" + jarDependencies +
                ", sourceDependencies=" + sourceDependencies +
                ", targetPath=" + targetPath +
                '}';
    }
}
