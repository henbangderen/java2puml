package henbangderen.java2puml.configuration;

import clojure.lang.*;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class EdnConfigurationParser
{
    private Logger log = LoggerFactory.getLogger(getClass());
    private final Keyword SOURCES = Keyword.intern(Symbol.create(null, "sources"));
    private final Keyword SKIP_SOURCES = Keyword.intern(Symbol.create(null, "skip-sources"));
    private final Keyword PACKAGES_TO_SKIP = Keyword.intern(Symbol.create(null, "packages-to-skip"));
    private final Keyword DEPENDENCIES = Keyword.intern(Symbol.create(null, "dependencies"));
    private final Keyword JARS = Keyword.intern(Symbol.create(null, "jars"));
    private final Keyword TARGET_PATH = Keyword.intern(Symbol.create(null, "target-path"));

    public Configuration parse(File ednConfigFile)
    {
        PersistentArrayMap configMap = read(ednConfigFile);

        Configuration config = new Configuration();
        config.setSources(convertToFiles(getVector(SOURCES, configMap)));
        config.setSkipSources(getVector(SKIP_SOURCES, configMap));
        config.setPackagesToSkip(getVector(PACKAGES_TO_SKIP, configMap));
        config.setJarDependencies(getDependencies(JARS, configMap));
        config.setSourceDependencies(getDependencies(SOURCES, configMap));
        config.setTargetPath(new File(getString(TARGET_PATH, configMap)));
        return config;
    }

    private List<File> getDependencies(Keyword key, PersistentArrayMap configMap)
    {
        List<File> result = new ArrayList<>();
        Map dependencies = getMap(DEPENDENCIES, configMap);
        List<String> sources = getVector(key, dependencies);
        for (String source : sources)
        {
            result.add(new File(source));
        }
        return result;
    }

    private List<File> convertToFiles(List<String> vector)
    {
        return vector.stream().map(File::new).collect(Collectors.toList());
    }

    private PersistentArrayMap read(File ednFilePath)
    {
        try
        {
            String config = FileUtils.readFileToString(ednFilePath, "UTF-8");
            Map<String, String> m = new HashMap<>();
            m.put("eof", null);
            IPersistentMap map = PersistentHashMap.create(m);
            return (PersistentArrayMap) EdnReader.readString(config, map);
        }
        catch (IOException e)
        {
            log.error("", e);
        }
        throw new IllegalStateException("Can not read '" + ednFilePath + "'");
    }

    private List<String> getVector(Keyword key, Map map)
    {
        return (List) map.getOrDefault(key, Collections.EMPTY_LIST);
    }

    private Map getMap(Keyword key, Map map)
    {
        return (Map) map.getOrDefault(key, Collections.EMPTY_MAP);
    }

    private String getString(Keyword key, PersistentArrayMap map)
    {
        return (String) map.get(key);
    }
}
