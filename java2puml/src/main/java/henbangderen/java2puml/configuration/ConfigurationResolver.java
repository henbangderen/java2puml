package henbangderen.java2puml.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Collects paths to files for setting up type solvers
 * JarTypeSolver asks for a full path to jar
 * JavaParserTypeSolver asks for source directory
 */
public class ConfigurationResolver
{
    private Logger log = LoggerFactory.getLogger(getClass());

    public Configuration resolve(Configuration raw)
    {
        Configuration result = new Configuration();
        List<File> sources = getFiles(raw.getSources(), ".java");

        result.setSources(filterFiles(sources, raw.getSkipSources()));
        result.setSourceDependencies(raw.getSourceDependencies());
        result.setPackagesToSkip(raw.getPackagesToSkip());
        result.setJarDependencies(getFiles(raw.getJarDependencies(), ".jar"));
        result.setTargetPath(raw.getTargetPath());
        return result;
    }

    private List<File> filterFiles(List<File> sources, List<String> skipSources)
    {
        if (skipSources.isEmpty())
        {
            return sources;
        }

        List<File> result = new ArrayList<>();
        for (File source : sources)
        {
            for (String skip : skipSources)
            {
                if (source.getAbsolutePath().contains(skip))
                {
                    continue;
                }
                result.add(source);
            }
        }
        return result;
    }

    private List<File> getFiles(List<File> dirsOrFiles, String extension)
    {
        List<File> files = getFiles(dirsOrFiles);
        return files.stream()
                .filter(f -> f.getName().endsWith(extension))
                .collect(Collectors.toList());
    }

    private List<File> getFiles(List<File> sources)
    {
        List<File> result = new ArrayList<>();
        for (File source : sources)
        {
            if (source.isFile())
            {
                result.add(source);
            } else
            {
                result.addAll(getFiles(source));
            }
        }
        return result;
    }

    private List<File> getFiles(File sourceDir)
    {
        try
        {
            return Files.walk(Paths.get(String.valueOf(sourceDir)))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
        }
        catch (IOException e)
        {
            log.error("Can not get files from: " + sourceDir, e);
        }
        return Collections.EMPTY_LIST;
    }
}
