package henbangderen.java2puml;

public interface Constants
{
    String CONFIGURATION_FILE_NAME = "configuration.edn";
    String CONFIGURATION_PATH_PROPERTY_NAME = "henbangderen.java2puml.configuration.path";
}
