package henbangderen.java2puml.configuration;

import junit.framework.Assert;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;


public class EdnConfigurationParserTest
{
    private Logger log = LoggerFactory.getLogger(getClass());
    private String TEST_SRC = "src/test/resource/EdnConfigurationParserTest";

    @Test
    public void configurationFromSystemProperty()
    {
        String configPath = TEST_SRC + "/configuration.edn";
        Configuration configuration = new EdnConfigurationParser().parse(new File(configPath));

        Assert.assertEquals("Configuration{sources=[A, B], packagesToSkip=[\"C\" \"D\"], skipSources=[], jarDependencies=[H], sourceDependencies=[E, F, G], targetPath=I}",
                String.valueOf(configuration));
    }
}