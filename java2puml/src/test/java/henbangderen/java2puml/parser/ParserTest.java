package henbangderen.java2puml.parser;

import henbangderen.java2puml.util.Constants;
import henbangderen.java2puml.model.ModelUnit;
import henbangderen.java2puml.model.Unit;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class ParserTest
{
    private Logger log = LoggerFactory.getLogger(getClass());
    private String TEST_SRC = "src/test/resource/ParserTest";

    private String stringFromFile(String filePath) throws IOException
    {
        return IOUtils.toString(new FileInputStream(new File(filePath)), "UTF-8");
    }

    private String qualifiedNames(Collection<Unit> units)
    {
        StringBuilder qualifiedNames = new StringBuilder();
        for (Unit unit : units)
        {
            qualifiedNames.append(unit.getDescribedType()).append('\n');
        }
        return qualifiedNames.toString();
    }

    @Test
    public void parse() throws IOException
    {
        Parser parser = new Parser();
        parser.setupTypeSolver(Constants.TEST_PROJECT_SRC);
        parser.setPackagesToSkip(Arrays.asList("java.lang", "java.util"));

        String filePath = Constants.TEST_PROJECT_SRC + Constants.PACKAGE_PATH + "/A.java";
        FileInputStream stream = new FileInputStream(filePath);
        ModelUnit modelUnit = parser.parse(stream);

        Unit extension = modelUnit.getExtension();
        Assert.assertEquals(
                stringFromFile(TEST_SRC + "/extensionA.expected"),
                                extension.toString());

        String implementation = qualifiedNames(modelUnit.getImplementations());
        Assert.assertEquals(
                stringFromFile(TEST_SRC + "/implementationsA.expected"),
                                implementation);

        String actual = qualifiedNames(modelUnit.getDependencies());
        String expected = stringFromFile(TEST_SRC + "/dependenciesA.expected");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void associationOnItself() throws IOException
    {
        Parser parser = new Parser();
        parser.setupTypeSolver(Constants.TEST_PROJECT_SRC);
        parser.setPackagesToSkip(Collections.EMPTY_LIST);

        String filePath = Constants.TEST_PROJECT_SRC + Constants.PACKAGE_PATH + "/composition/B.java";
        FileInputStream stream = new FileInputStream(filePath);
        ModelUnit modelUnit = parser.parse(stream);

        String associations = qualifiedNames(modelUnit.getAssociations());
        Assert.assertEquals("java.util.Map<henbangderen.java2puml.test.composition.B, henbangderen.java2puml.test.aggregation.C>\n", associations);
    }
}