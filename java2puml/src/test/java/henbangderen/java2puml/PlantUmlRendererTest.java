package henbangderen.java2puml;

import henbangderen.java2puml.model.ModelUnit;
import henbangderen.java2puml.model.Unit;
import henbangderen.java2puml.model.UnitBuilder;
import henbangderen.java2puml.renderer.PlantUmlRenderer;
import henbangderen.java2puml.util.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

//todo: load model from xmls
public class PlantUmlRendererTest
{
    private Logger log = LoggerFactory.getLogger(getClass());
    private String TEST_SRC = "src/test/resource/PlantUmlRendererTest";
    private UnitBuilder builder = new UnitBuilder();

    private ModelUnit generateModelUnit(String rootClassName, String suffix)
    {
        ModelUnit model = new ModelUnit();
        model.setRoot(builder.build(rootClassName));
        model.setExtension(builder.build("com.scatola.E" + suffix));

        List<Unit> implementations = new ArrayList<>();
        implementations.add(builder.build("com.scatola.B" + suffix));
        implementations.add(builder.build("com.tavolo.C" + suffix));
        model.setImplementations(implementations);

        List<Unit> dependencies = new ArrayList<>();
        dependencies.add(builder.build("com.scatola.D" + suffix));
        dependencies.add(builder.build("com.tavolo.E" + suffix));
        model.setDependencies(dependencies);
        return model;
    }

    @Test
    public void oneClass()
    {
        PlantUmlRenderer renderer = new PlantUmlRenderer();
        ModelUnit model = generateModelUnit("A", "");
        renderer.setModelUnit(model);

        String actual = renderer.render();
        String expected = Utils.stringFromFile(TEST_SRC + "/oneClass.expected");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void twoClasses()
    {
        PlantUmlRenderer renderer = new PlantUmlRenderer();
        ModelUnit model = generateModelUnit("A", "");
        renderer.setModelUnit(model);
        String one = renderer.render();

        ModelUnit model2 = generateModelUnit("com.scatola.D", "d");
        renderer.setModelUnit(model2);
        String two = renderer.render();

        String actual = one + "\n" + two;

        String expected = Utils.stringFromFile(TEST_SRC + "/twoClasses.expected");
        Assert.assertEquals(expected, actual);
    }
}