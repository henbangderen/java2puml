package henbangderen.java2puml.util;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public interface Utils
{
    Logger log = LoggerFactory.getLogger(Utils.class);

    static String stringFromFile(String filePath)
    {
        try
        {
            return IOUtils.toString(new FileInputStream(new File(filePath)), "UTF-8");
        }
        catch (IOException e)
        {
            log.error("can not get string from '" + filePath + "'", e);
            throw new IllegalStateException("can not get string from '" + filePath + "'");
        }
    }
}
