package henbangderen.java2puml.core.revived;

import henbangderen.java2puml.Processor;
import henbangderen.java2puml.configuration.Configuration;
import henbangderen.java2puml.util.Constants;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;

public class MainTest
{
    @Test
    public void smokeTest()
    {
        Configuration configuration = new Configuration();
        configuration.setSources(Arrays.asList(new File
                (Constants.TEST_PROJECT_SRC + Constants.PACKAGE_PATH + "/association/A.java")));
        configuration.setSourceDependencies(Arrays.asList(new File (Constants.TEST_PROJECT_SRC)));
        new Processor().setConfiguration(configuration).process();
        //todo: add assertions
    }
}
