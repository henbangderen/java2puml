package henbangderen.java2puml;

import henbangderen.java2puml.configuration.Configuration;
import henbangderen.java2puml.configuration.ConfigurationFactory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcessorTest
{
    private Logger log = LoggerFactory.getLogger(getClass());
    private String TEST_SRC = "src/test/resource/ProcessorTest";

    @Test
    public void java2umlRevivedTestProject()
    {
        test("java2uml-revived-test.edn");
        //todo: add assertion
    }

    //@Test
    // download sources with dependencies
    public void zxstream()
    {
        System.setProperty(Constants.CONFIGURATION_PATH_PROPERTY_NAME,
                "..examples/xstream.edn");
        Main.main(null);
    }

    public void test(String configuration)
    {
        String configPath = TEST_SRC + "/" + configuration;
        System.setProperty(Constants.CONFIGURATION_PATH_PROPERTY_NAME, configPath);
        Configuration config = ConfigurationFactory.getInstance().getConfiguration();

        Processor processor = new Processor();
        processor.setConfiguration(config).process();
    }
}
